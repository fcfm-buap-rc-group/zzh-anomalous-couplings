# ZZH Anomalous Couplings
## Alan Hernández FCFM-BUAP Radiative Corrections Group, October 2022

This repository contains the anomalous coupling $h_2^V$ defined in https://arxiv.org/abs/2301.13127. 
The $h_2^V$ form factor arise from three diferent contributions: $\mathcal{F}$ (Fermion contribution), $\mathcal{W}$ (W boson contribution) and $\mathcal{HZ}$ (H and Z bosons contributions). We present the expressions in terms of the scalar Passarino-Veltman functions for both cases $ZZH^\ast$ and $HZZ^\ast$. The total SM contribution to $h_2^V$ can be written as:
$$h_2^V(s^2)=\mathcal{F}+\mathcal{W}+\mathcal{HZ},$$
For more details see: https://arxiv.org/abs/2301.13127.

The files are in Mathemathica notebooks, and in LoopTools notation. 

## Notation 

We use the following notation:
$m_f$-fermions mass,
$N_f$-Number of flavors, $t$-fermions weak isospin,
$q$- electric charge,
$sw2$- squared sine of the Weinberg angle, 
$cw$- cosine of the Weinberg angle, 
$mz$-Z boson mass, 
$mw$- W boson mass, 
H-Higgs boson mass.

## Authors and acknowledgment
Please cite https://arxiv.org/abs/2301.13127 if our repository  is used in your research.

## Support and comments
For any questions, please contact Alan I. Hernández (alaban7_3@hotmail) FCFM-BUAP Radiative Corrections Group leader, October 2022.

